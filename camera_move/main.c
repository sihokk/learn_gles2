#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <GLES2/gl2.h>

#include <cglm/cglm.h>

#define INIT_SCREEN_WIDTH 480
#define INIT_SCREEN_HEIGHT 480

#define CAMERA_MOVE_SPEED 0.01f // distance in 1 ms

typedef struct
{
    GLint format;
    int width;
    int height;
    void *pixels;
} texture_image_t;

static char *read_shader(const char *file);
static bool load_shader(GLenum type, const char *path, GLuint *shader);
static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog);

static void load_textures(const char *path0, const char *path1, GLuint *textures);
static texture_image_t load_texture_image(const char *path);
static GLuint load_texture(const char *path);

static struct
{
    int32_t screen_width;
    int32_t screen_height;
    GLuint program;
    GLuint texture0;
    GLuint texture1;
    uint32_t frame_interval;
    vec3 camera_pos;
    vec3 camera_front;
    vec3 camera_up;
} render_state = {0};

static void on_resize(int32_t width, int32_t height)
{

    if (width > 0 && height > 0)
    {
        render_state.screen_width = width;
        render_state.screen_height = height;

        glViewport(0, 0, width, height);
    }
}

static void calc_frame_interval()
{

    static struct
    {
        bool valid;
        uint32_t timestamp;
    } last_frame = {false};

    const uint32_t now = SDL_GetTicks();
    if (last_frame.valid)
    {
        render_state.frame_interval = now - last_frame.timestamp;
        last_frame.timestamp = now;
    }
    else
    {
        last_frame.valid = true;
        last_frame.timestamp = now;
        render_state.frame_interval = 0;
    }
}

static void camera_reset()
{
    vec3 pos = {0, 0, 3};
    glm_vec3_copy(pos, render_state.camera_pos);

    vec3 orig = GLM_VEC3_ZERO_INIT;
    glm_vec3_sub(orig, pos, render_state.camera_front);
    glm_vec3_normalize(render_state.camera_front);

    vec3 up = {0, 1, 0}; // Y
    glm_vec3_copy(up, render_state.camera_up);
}

static void camera_move_right(float dist)
{
    render_state.camera_pos[0] += dist;
}

static void camera_move_forward(float dist)
{
    render_state.camera_pos[2] -= dist;
}

static void render_init()
{

    glEnable(GL_DEPTH_TEST);

    GLuint prog;
    if (!load_program("vertex_shader.glsl.c", "frag_shader.glsl.c", &prog))
    {
        printf("Loading program failed\n");
        return;
    }

    render_state.program = prog;

    const GLfloat vertices[] = {
#include "vertices.h"
    };

    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLint attr_pos = glGetAttribLocation(prog, "a_pos");
    GLint attr_tex = glGetAttribLocation(prog, "a_tex");

    const GLsizei stride = 5 * sizeof(GLfloat);

    glEnableVertexAttribArray(attr_pos);
    glVertexAttribPointer(attr_pos, 3, GL_FLOAT, GL_FALSE, stride, 0);

    glEnableVertexAttribArray(attr_tex);
    glVertexAttribPointer(attr_tex, 2, GL_FLOAT, GL_FALSE, stride, (void *)(3 * sizeof(GLfloat)));

    // Load texture objects
    GLuint tex0, tex1;
    {
        GLuint a[2];
        load_textures("container.jpg", "awesomeface.png", a);
        tex0 = a[0];
        tex1 = a[1];
        render_state.texture0 = tex0;
        render_state.texture1 = tex1;
    }

    GLint uni_tex = glGetUniformLocation(prog, "s_tex");
    GLint uni_tex1 = glGetUniformLocation(prog, "s_tex1");

    // Use program before setting uniform/sampler value
    glUseProgram(prog);

    glUniform1i(uni_tex, 0);
    glUniform1i(uni_tex1, 1);

    // Bind the 1st texture object to unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex0);
    // Bind the 2nd texture object to unit 1
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tex1);

    glClearColor(.2f, .3f, .3f, 1.f);

    // Camera state
    camera_reset();
}

static void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const vec3 cube_positions[] = {
        {0.0f, 0.0f, 0.0f},
        {2.0f, 5.0f, -15.0f},
        {-1.5f, -2.2f, -2.5f},
        {-3.8f, -2.0f, -12.3f},
        {2.4f, -0.4f, -3.5f},
        {-1.7f, 3.0f, -7.5f},
        {1.3f, -2.0f, -2.5f},
        {1.5f, 2.0f, -2.5f},
        {1.5f, 0.2f, -1.5f},
        {-1.3f, 1.0f, -1.5f},
    };

    for (int i = 0, cnt = sizeof(cube_positions) / sizeof(vec3); i < cnt; ++i)
    {

        mat4 m = GLM_MAT4_IDENTITY_INIT;

        // Model transform: place cube
        {
            mat4 m1 = GLM_MAT4_IDENTITY_INIT;
            glm_translate(m, cube_positions[i]);

            glm_mat4_mul(m1, m, m);
        }

        // View transform: camera movement
        {
            mat4 m1;
            vec3 target;
            glm_vec3_add(render_state.camera_pos, render_state.camera_front, target);
            glm_lookat(render_state.camera_pos, target, render_state.camera_up, m1);

            glm_mat4_mul(m1, m, m);
        }

        // Projection
        {
            mat4 m1;
            glm_perspective(glm_rad(45), render_state.screen_width / (float)render_state.screen_height,
                            .1, 100, m1);

            glm_mat4_mul(m1, m, m);
        }

        GLuint prog = render_state.program;
        GLint loc = glGetUniformLocation(prog, "m_trans");
        glUniformMatrix4fv(loc, 1, GL_FALSE, (GLfloat *)m);

        glDrawArrays(GL_TRIANGLES, 0, 36);
    } // for
}

int main(int argc, char **argv)
{

    if (0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER))
    {
        printf("SDL init failed. \n");
        return -1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 8);

    SDL_Window *win = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (NULL == win)
    {
        printf("SDL window creation failed. \n");
        SDL_Quit();
        return -1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(win);
    if (NULL == context)
    {
        printf("SDL OpenGL context creation failed. \n");
        SDL_Quit();
        return -1;
    }

    render_state.screen_width = INIT_SCREEN_WIDTH;
    render_state.screen_height = INIT_SCREEN_HEIGHT;

    glViewport(0, 0, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT);
    render_init();

    while (1)
    {
        calc_frame_interval();
        render();
        SDL_GL_SwapWindow(win);

        SDL_Event e;
        if (0 == SDL_PollEvent(&e))
        {
            continue;
        }

        // Key press: camera movement
        if (SDL_KEYDOWN == e.type)
        {

            const SDL_Keycode k = e.key.keysym.sym;

            if (SDLK_w == k)
            {
                const float dist = CAMERA_MOVE_SPEED * render_state.frame_interval;
                camera_move_forward(dist);
            }
            else if (SDLK_s == k)
            {
                const float dist = CAMERA_MOVE_SPEED * render_state.frame_interval;
                camera_move_forward(-dist);
            }
            else if (SDLK_a == k)
            {
                const float dist = CAMERA_MOVE_SPEED * render_state.frame_interval;
                camera_move_right(-dist);
            }
            else if (SDLK_d == k)
            {
                const float dist = CAMERA_MOVE_SPEED * render_state.frame_interval;
                camera_move_right(dist);
            }

            continue;
        }

        if (SDL_KEYUP == e.type)
        {
            const SDL_Keycode k = e.key.keysym.sym;

            // ESC: quit
            if (SDLK_ESCAPE == k)
            {
                break;
            }

            if (SDLK_PERIOD == k || SDLK_KP_PERIOD == k)
            {
                camera_reset();
            }

            continue;
        }

        if (SDL_WINDOWEVENT == e.type)
        {
            // Window closed
            if (SDL_WINDOWEVENT_CLOSE == e.window.event)
            {
                break;
            }

            if (SDL_WINDOWEVENT_RESIZED == e.window.event)
            {
                on_resize(e.window.data1, e.window.data2);
            }

            continue;
        }
    }

    SDL_Quit();

    return 0;
}

static bool load_shader(GLenum type, const char *path, GLuint *shader)
{
    GLuint shader_id = glCreateShader(type);
    char *src = read_shader(path);

    glShaderSource(shader_id, 1, &src, NULL);
    glCompileShader(shader_id);

    free(src);

    // Print compile log
    {
        GLint len;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetShaderInfoLog(shader_id, len, NULL, buf);

            printf("Compiling %s : %s\n", path, buf);
            free(buf);
        }
    }

    GLint result;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (result)
    {
        // Success
        *shader = shader_id;
        return true;
    }
    else
    {
        glDeleteShader(shader_id);
        return false;
    }
}

static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog)
{

    GLuint prog_id = glCreateProgram();

    GLuint vertex_shader, frag_shader;

    if (!load_shader(GL_VERTEX_SHADER, vertex_shader_path, &vertex_shader))
    {
        printf("Loading vertex shader failed\n");
        glDeleteProgram(prog_id);
        return false;
    }

    if (!load_shader(GL_FRAGMENT_SHADER, frag_shader_path, &frag_shader))
    {
        printf("Loading fragment shader failed\n");
        glDeleteShader(vertex_shader);
        glDeleteProgram(prog_id);
        return false;
    }

    glAttachShader(prog_id, vertex_shader);
    glAttachShader(prog_id, frag_shader);
    glLinkProgram(prog_id);

    // It's safe to delete them
    glDeleteShader(vertex_shader);
    glDeleteShader(frag_shader);

    // Print linking log
    {
        GLint len;
        glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetProgramInfoLog(prog_id, len, NULL, buf);

            printf("Linking program : %s\n", buf);
            free(buf);
        }
    }

    GLint result;
    glGetProgramiv(prog_id, GL_LINK_STATUS, &result);
    if (result)
    {
        *prog = prog_id;
        return true;
    }
    else
    {
        glDeleteProgram(prog_id);
        return false;
    }
}

static char *read_shader(const char *file)
{

    SDL_RWops *in = SDL_RWFromFile(file, "r");
    if (NULL == in)
    {
    }

    const size_t delta = 512;

    char *buf = (char *)malloc(delta);
    size_t len = 0;
    size_t cap = delta;

    while (1)
    {
        size_t len1 = SDL_RWread(in, buf + len, 1, cap - len);
        if (0 == len1)
        {
            // TODO: may be error
            break;
        }

        len += len1;

        if (cap == len)
        {
            // Increase buffer
            char *buf1 = (char *)realloc(buf, cap + delta);
            if (NULL == buf1)
            {
                // TODO
            }

            buf = buf1;
            cap += delta;
        }
    }

    buf[len] = '\0';

    return buf;
}

static void load_textures(const char *path0, const char *path1, GLuint *textures)
{
    //
    {
        const int flags = IMG_INIT_JPG | IMG_INIT_PNG;
        if (flags != IMG_Init(flags))
        {
            // TODO
        }
    }

    // Use tightly packed data
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    GLuint tex0 = load_texture(path0);
    GLuint tex1 = load_texture(path1);

    IMG_Quit();

    textures[0] = tex0;
    textures[1] = tex1;
}

static GLuint load_texture(const char *path)
{

    texture_image_t img = load_texture_image(path);

    GLuint tex;
    glGenTextures(1, &tex);

    // Active unit does not matter here. We just need to copy texture data to texture object
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, img.format, img.width, img.height, 0, img.format, GL_UNSIGNED_BYTE, img.pixels);

    // Set the filtering mode
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    free(img.pixels);

    return tex;
}

static texture_image_t load_texture_image(const char *path)
{
    SDL_Surface *surf;
    {
        surf = IMG_Load(path);
        if (NULL == surf)
        {
            // TODO
        }
    }

    // printf("Pixel format: %s\n", SDL_GetPixelFormatName(surf->format->format));

    int w, h;
    void *pixels;
    {
        w = surf->w;
        h = surf->h;

        pixels = malloc(4 * w * h);

        int err = SDL_ConvertPixels(w, h, surf->format->format, surf->pixels, surf->pitch, SDL_PIXELFORMAT_RGBA32, pixels, w * 4);
        if (err)
        {
            printf("Convert pixels failed: %s\n", SDL_GetError());
        }
    }

    SDL_FreeSurface(surf);

    //  Flip vertically..
    {
        const int stride = 4 * w;
        void *buf = malloc(stride);

        for (int r = 0, max = h / 2; r < max; ++r)
        {
            int r1 = h - r - 1;
            void *p = (uint8_t *)(pixels) + r * stride;
            void *p1 = (uint8_t *)(pixels) + r1 * stride;
            memcpy(buf, p, stride);
            memcpy(p, p1, stride);
            memcpy(p1, buf, stride);
        }

        free(buf);
    }

    texture_image_t img = {
        .format = GL_RGBA,
        .width = w,
        .height = h,
        .pixels = pixels,
    };

    return img;
}
