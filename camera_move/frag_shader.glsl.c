
#version 100

precision mediump float;

varying vec2 v_tex;

uniform sampler2D s_tex;
uniform sampler2D s_tex1;

void main()
{
    gl_FragColor = mix(texture2D(s_tex, v_tex), texture2D(s_tex1, v_tex), 0.2);
}
