
#version 100

// Required. Otherwise compiling will fail
// Fragment shaders do NOT have a default precision. Have to delcare explicitly
precision mediump float;

varying vec4 v_color;
varying vec2 v_tex;

uniform sampler2D s_tex;

void main()
{
    // gl_FragColor = v_color;
    // gl_FragColor = texture2D(s_tex, v_tex);
    gl_FragColor = texture2D(s_tex, v_tex) * v_color;
}
