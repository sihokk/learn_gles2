#include <stdio.h>
#include <stdint.h>

#include <SDL2/SDL.h>
#include <GLES2/gl2.h>

#define INIT_SCREEN_WIDTH 320
#define INIT_SCREEN_HEIGHT 320

static void on_resize(int32_t width, int32_t height)
{
    glViewport(0, 0, width, height);
}

static void render()
{
    glClearColor(.2f, .3f, .3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
}

int main(int argc, char **argv)
{

    if (0 != SDL_Init(SDL_INIT_VIDEO))
    {
        printf("SDL init failed. \n");
        return -1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_Window *win = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (NULL == win)
    {
        printf("SDL window creation failed. \n");
        SDL_Quit();
        return -1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(win);
    if (NULL == context)
    {
        printf("SDL OpenGL context creation failed. \n");
        SDL_Quit();
        return -1;
    }

    glViewport(0, 0, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT);

    while (1)
    {

        render();
        SDL_GL_SwapWindow(win);

        SDL_Event e;
        if (0 == SDL_PollEvent(&e))
        {
            continue;
        }

        // ESC: quit
        if (SDL_KEYUP == e.type && SDLK_ESCAPE == e.key.keysym.sym)
        {
            break;
        }

        if (SDL_WINDOWEVENT == e.type)
        {
            // Window closed
            if (SDL_WINDOWEVENT_CLOSE == e.window.event)
            {
                break;
            }

            if (SDL_WINDOWEVENT_RESIZED == e.window.event)
            {
                on_resize(e.window.data1, e.window.data2);
            }
        }
    }

    SDL_Quit();

    return 0;
}
