
#version 100

// Required. Otherwise compiling will fail
// Fragment shaders do NOT have a default precision. Have to delcare explicitly
precision mediump float;

varying vec4 vertex_color;

void main()
{
    gl_FragColor = vertex_color;
}
