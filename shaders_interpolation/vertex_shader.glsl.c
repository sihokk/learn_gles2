
#version 100

attribute vec4 a_pos;
attribute mediump vec4 a_color;

varying mediump vec4 vertex_color;

void main()
{
    gl_Position = a_pos;
    vertex_color = a_color;
}
