
#version 100

precision mediump float;

varying vec4 v_color;
varying vec2 v_tex;

uniform sampler2D s_tex;
uniform sampler2D s_tex1;

// --= Original Porter-Duff operations =--

float PorterDuff_Op(float a, float fa, float b, float fb);
vec4 PorterDuff_Op(vec4 a, float fa, vec4 b, float fb);
vec4 PorterDuff_Clear(vec4 a, vec4 b);
vec4 PorterDuff_A(vec4 a, vec4 b);
vec4 PorterDuff_A_Over_B(vec4 a, vec4 b);
vec4 PorterDuff_A_In_B(vec4 a, vec4 b);
vec4 PorterDuff_A_Out_B(vec4 a, vec4 b);
vec4 PorterDuff_A_Atop_B(vec4 a, vec4 b);
vec4 PorterDuff_A_Xor_B(vec4 a, vec4 b);
vec4 PorterDuff_A_Plus_B(vec4 a, vec4 b);

// Uniary operations
vec4 PorterDuff_Darken(vec4 a, float f);
vec4 PorterDuff_Dissolve(vec4 a, float f);
vec4 PorterDuff_Opaque(vec4 a, float f);

// --= Extended operations =--
// See https://www.android-doc.com/reference/android/graphics/PorterDuff.Mode.html

vec4 PorterDuff_Darken(vec4 a, vec4 b);
vec4 PorterDuff_Lighten(vec4 a, vec4 b);
vec4 PorterDuff_Multiply(vec4 a, vec4 b);
vec4 PorterDuff_Overlay(vec4 a, vec4 b);
vec4 PorterDuff_Screen(vec4 a, vec4 b);

void main()
{
    // gl_FragColor = texture2D(s_tex1, v_tex) * v_color;
    // gl_FragColor = mix(texture2D(s_tex, v_tex), texture2D(s_tex1, v_tex), 0.2);
    vec4 a = texture2D(s_tex, v_tex);
    vec4 b = texture2D(s_tex1, v_tex);
    gl_FragColor = PorterDuff_A_Over_B(a, b);
}

float PorterDuff_Op(float a, float fa, float b, float fb)
{
    return a * fa + b * fb;
}

vec4 PorterDuff_Op(vec4 a, float fa, vec4 b, float fb)
{
    return a * fa + b * fb;
}

vec4 PorterDuff_Clear(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 0., b, 0.);
}

vec4 PorterDuff_A(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 1., b, 0.);
}

vec4 PorterDuff_A_Over_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 1., b, 1. - a.a);
}

vec4 PorterDuff_A_In_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, b.a, b, 0.);
}

vec4 PorterDuff_A_Out_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 1. - b.a, b, 0.);
}

vec4 PorterDuff_A_Atop_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, b.a, b, 1. - a.a);
}

vec4 PorterDuff_A_Xor_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 1. - b.a, b, 1. - a.a);
}

vec4 PorterDuff_A_Plus_B(vec4 a, vec4 b)
{
    return PorterDuff_Op(a, 1., b, 1.);
}

vec4 PorterDuff_Darken(vec4 a, float f)
{
    return vec4(vec3(a) * f, a.a);
}

vec4 PorterDuff_Dissolve(vec4 a, float f)
{
    return a * f;
}

vec4 PorterDuff_Opaque(vec4 a, float f)
{
    return vec4(vec3(a), a.a * f);
}

vec4 PorterDuff_Darken(vec4 a, vec4 b)
{
    // [Sa + Da - Sa*Da, Sc*(1 - Da) + Dc*(1 - Sa) + min(Sc, Dc)]

    return vec4(a.r * (1. - b.a) + b.r * (1. - a.a) + min(a.r, b.r), // r
                a.g * (1. - b.a) + b.g * (1. - a.a) + min(a.g, b.g), // g
                a.b * (1. - b.a) + b.b * (1. - a.a) + min(a.b, b.b), // b
                a.a + b.a - a.a * b.a                                // a
    );
}

vec4 PorterDuff_Lighten(vec4 a, vec4 b)
{
    // [Sa + Da - Sa*Da, Sc*(1 - Da) + Dc*(1 - Sa) + max(Sc, Dc)]

    return vec4(a.r * (1. - b.a) + b.r * (1. - a.a) + max(a.r, b.r), // r
                a.g * (1. - b.a) + b.g * (1. - a.a) + max(a.g, b.g), // g
                a.b * (1. - b.a) + b.b * (1. - a.a) + max(a.b, b.b), // b
                a.a + b.a - a.a * b.a                                // a
    );
}

vec4 PorterDuff_Multiply(vec4 a, vec4 b)
{
    // [Sa * Da, Sc * Dc]
    return a * b;
}

vec4 PorterDuff_Overlay(vec4 a, vec4 b)
{
    // TODO
    return a;
}

vec4 PorterDuff_Screen(vec4 a, vec4 b)
{
    // [Sa + Da - Sa * Da, Sc + Dc - Sc * Dc]
    return a + b - a * b;
}
