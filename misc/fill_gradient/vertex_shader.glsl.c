
#version 100

struct anchor
{
    vec2 pos;
    vec3 color;
};

attribute vec4 a_pos;

uniform mat4 m_trans;
uniform anchor u_anchors[2];

varying mediump vec4 v_color;

vec3 gradient(vec2 pos);

void main()
{
    gl_Position = m_trans * a_pos;
    v_color = vec4(gradient(vec2(a_pos)), 1.0);
}

vec3 gradient(vec2 pos)
{
    float x0 = u_anchors[0].pos.x;
    float y0 = u_anchors[0].pos.y;
    float x1 = u_anchors[1].pos.x;
    float y1 = u_anchors[1].pos.y;

    float A = x1 - x0;
    float B = y1 - y0;
    float C = B * x0 - A * y0;
    float D = A * pos.x + B * pos.y;

    float d = A * A + B * B;
    float x = (A * D + B * C) / d;
    float y = (B * D - A * C) / d;

    float t = (x - x0 + y - y0) / (x1 - x0 + y1 - y0);
    vec3 c = u_anchors[0].color + (u_anchors[1].color - u_anchors[0].color) * t;
    return clamp(c, 0.0, 1.0);
}
