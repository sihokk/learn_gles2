#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include <math.h>

#include <SDL2/SDL.h>
#include <GLES2/gl2.h>

#include <cglm/cglm.h>

#define INIT_SCREEN_WIDTH 480
#define INIT_SCREEN_HEIGHT 480

#define IMG_WIDTH 480
#define IMG_HEIGHT 480

static char *read_shader(const char *file);
static bool load_shader(GLenum type, const char *path, GLuint *shader);
static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog);

static void flip_vertically(GLfloat *v, int dim, int count, float height);

static struct
{
    GLuint program;
} render_state = {0};

static void on_resize(int32_t width, int32_t height)
{
    if (width > 0 && height > 0)
    {
        glViewport(0, 0, width, height);
    }
}

static void render_init()
{

    GLuint prog;
    if (!load_program("vertex_shader.glsl.c", "frag_shader.glsl.c", &prog))
    {
        printf("Loading program failed\n");
        return;
    }

    render_state.program = prog;

    GLfloat vertices[] = {
        194, 350, //
        407, 297, //
        444, 166, //
        331, 108, //
        246, 147, //
        199, 77,  //
        90, 75,   //
        44, 248,  //
    };

    GLfloat anchors[] = {
        91, 295, .976, .082, .067,  //
        401, 170, .875, .922, .008, //
    };

    flip_vertically(vertices, 2, sizeof(vertices) / sizeof(GLfloat), IMG_HEIGHT);
    flip_vertically(anchors, 5, 2, IMG_HEIGHT);

    //
    {
        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        GLint loc = glGetAttribLocation(prog, "a_pos");

        glEnableVertexAttribArray(loc);
        glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, 0);
    }

    glUseProgram(prog);

    //
    {
        mat4 m;
        vec3 eye = {IMG_WIDTH / 2, IMG_HEIGHT / 2, 1};
        vec3 dir = {0, 0, -1};
        vec3 up = {0, 1, 0};
        glm_look(eye, dir, up, m);

        mat4 m1;
        glm_ortho(-IMG_WIDTH / 2, IMG_WIDTH / 2, -IMG_HEIGHT / 2, IMG_HEIGHT / 2, 0.1, 10, m1);
        glm_mat4_mul(m1, m, m);

        GLint loc = glGetUniformLocation(prog, "m_trans");
        glUniformMatrix4fv(loc, 1, GL_FALSE, m[0]);
    }

    for (int i = 0; i < 2; ++i)
    {
        char buf[64];
        GLint loc;

        sprintf(buf, "u_anchors[%d].pos", i);
        loc = glGetUniformLocation(prog, buf);
        glUniform2fv(loc, 1, anchors + 5 * i);

        sprintf(buf, "u_anchors[%d].color", i);
        loc = glGetUniformLocation(prog, buf);
        glUniform3fv(loc, 1, anchors + 5 * i + 2);
    }

    glClearColor(1, 1, 1, 1);
}

static void render()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 8);
}

int main(int argc, char **argv)
{

    if (0 != SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER))
    {
        printf("SDL init failed. \n");
        return -1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_Window *win = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (NULL == win)
    {
        printf("SDL window creation failed. \n");
        SDL_Quit();
        return -1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(win);
    if (NULL == context)
    {
        printf("SDL OpenGL context creation failed. \n");
        SDL_Quit();
        return -1;
    }

    glViewport(0, 0, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT);
    render_init();

    while (1)
    {

        render();
        SDL_GL_SwapWindow(win);

        SDL_Event e;
        if (0 == SDL_PollEvent(&e))
        {
            continue;
        }

        // ESC: quit
        if (SDL_KEYUP == e.type && SDLK_ESCAPE == e.key.keysym.sym)
        {
            break;
        }

        if (SDL_WINDOWEVENT == e.type)
        {
            // Window closed
            if (SDL_WINDOWEVENT_CLOSE == e.window.event)
            {
                break;
            }

            if (SDL_WINDOWEVENT_RESIZED == e.window.event)
            {
                on_resize(e.window.data1, e.window.data2);
            }
        }
    }

    SDL_Quit();

    return 0;
}

static bool load_shader(GLenum type, const char *path, GLuint *shader)
{
    GLuint shader_id = glCreateShader(type);
    char *src = read_shader(path);

    glShaderSource(shader_id, 1, &src, NULL);
    glCompileShader(shader_id);

    free(src);

    // Print compile log
    {
        GLint len;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetShaderInfoLog(shader_id, len, NULL, buf);

            printf("Compiling %s : %s\n", path, buf);
            free(buf);
        }
    }

    GLint result;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (result)
    {
        // Success
        *shader = shader_id;
        return true;
    }
    else
    {
        glDeleteShader(shader_id);
        return false;
    }
}

static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog)
{

    GLuint prog_id = glCreateProgram();

    GLuint vertex_shader, frag_shader;

    if (!load_shader(GL_VERTEX_SHADER, vertex_shader_path, &vertex_shader))
    {
        printf("Loading vertex shader failed\n");
        glDeleteProgram(prog_id);
        return false;
    }

    if (!load_shader(GL_FRAGMENT_SHADER, frag_shader_path, &frag_shader))
    {
        printf("Loading fragment shader failed\n");
        glDeleteShader(vertex_shader);
        glDeleteProgram(prog_id);
        return false;
    }

    glAttachShader(prog_id, vertex_shader);
    glAttachShader(prog_id, frag_shader);
    glLinkProgram(prog_id);

    // It's safe to delete them
    glDeleteShader(vertex_shader);
    glDeleteShader(frag_shader);

    // Print linking log
    {
        GLint len;
        glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetProgramInfoLog(prog_id, len, NULL, buf);

            printf("Linking program : %s\n", buf);
            free(buf);
        }
    }

    GLint result;
    glGetProgramiv(prog_id, GL_LINK_STATUS, &result);
    if (result)
    {
        *prog = prog_id;
        return true;
    }
    else
    {
        glDeleteProgram(prog_id);
        return false;
    }
}

static char *read_shader(const char *file)
{

    SDL_RWops *in = SDL_RWFromFile(file, "r");
    if (NULL == in)
    {
    }

    const size_t delta = 512;

    char *buf = (char *)malloc(delta);
    size_t len = 0;
    size_t cap = delta;

    while (1)
    {
        size_t len1 = SDL_RWread(in, buf + len, 1, cap - len);
        if (0 == len1)
        {
            // TODO: may be error
            break;
        }

        len += len1;

        if (cap == len)
        {
            // Increase buffer
            char *buf1 = (char *)realloc(buf, cap + delta);
            if (NULL == buf1)
            {
                // TODO
            }

            buf = buf1;
            cap += delta;
        }
    }

    buf[len] = '\0';

    return buf;
}

static void flip_vertically(GLfloat *v, int dim, int count, float height)
{
    for (int i = 0; i < count; ++i)
    {
        v[1] = height - v[1];
        v += dim;
    }
}