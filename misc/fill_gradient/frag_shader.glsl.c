
#version 100

// Required. Otherwise compiling will fail
// Fragment shaders do NOT have a default precision. Have to delcare explicitly
precision mediump float;

varying vec4 v_color;

void main()
{
    gl_FragColor = v_color;
}
