#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <GLES2/gl2.h>

#define WINDOW_WIDTH 480
#define WINDOW_HEIGHT 480

PFNEGLQUERYDEVICESEXTPROC eglQueryDevicesEXT;

static void render()
{
    glClearColor(.2f, .9f, .3f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);
}

static void output_image()
{
    GLubyte buf[3 * WINDOW_WIDTH * WINDOW_HEIGHT];
    glReadPixels(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, buf);

    printf("P3\n");
    printf("%d %d\n", WINDOW_WIDTH, WINDOW_HEIGHT);
    printf("255\n");

    for (int r = 0; r < WINDOW_HEIGHT; ++r)
    {
        const GLubyte *p = buf + r * 3 * WINDOW_WIDTH;
        for (int i = 0; i < 3 * WINDOW_WIDTH; ++i)
        {
            printf(" %d", 0xff & p[i]);
        }
        printf("\n");
    }
}

int main(int argc, char **argv)
{
    //
    {
        const char *s = eglQueryString(EGL_NO_DISPLAY, EGL_EXTENSIONS);
        if (NULL == strstr(s, "EGL_EXT_platform_device") || NULL == strstr(s, "EGL_EXT_device_base"))
        {
            printf("EGL extension EXT_platform_device not supported\n");
            exit(-1);
        }

        eglQueryDevicesEXT = (PFNEGLQUERYDEVICESEXTPROC)eglGetProcAddress("eglQueryDevicesEXT");
    }

    //
    EGLDeviceEXT egl_device;
    {
        EGLint num;
        eglQueryDevicesEXT(0, NULL, &num);

        // printf("%d EGL devices\n", num);

        if (0 == num)
        {
            printf("No EGL device supported\n");
            exit(-1);
        }

        // EGLDeviceEXT *devices = (EGLDeviceEXT *)malloc(num * sizeof(EGLDeviceEXT));
        eglQueryDevicesEXT(1, &egl_device, &num);

        // egl_device = devices[0];
        // free(devices);
    }

    EGLDisplay egl_disp = eglGetPlatformDisplay(EGL_PLATFORM_DEVICE_EXT, egl_device, NULL);
    if (EGL_NO_DISPLAY == egl_disp)
    {
        printf("Cannot create EGL display\n");
    }

    if (!eglInitialize(egl_disp, NULL, NULL))
    {
        printf("EGL initialization failed\n");
    }

    EGLConfig egl_config;
    {

        const EGLint attr_list[] = {
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, //
            EGL_SURFACE_TYPE, EGL_PBUFFER_BIT,       //
            EGL_COLOR_BUFFER_TYPE, EGL_RGB_BUFFER,   //
            EGL_RED_SIZE, 5,                         //
            EGL_GREEN_SIZE, 6,                       //
            EGL_BLUE_SIZE, 5,                        //
            EGL_DEPTH_SIZE, 8,                       //
            EGL_NONE,                                //
        };

        EGLint num;
        if (!eglChooseConfig(egl_disp, attr_list, &egl_config, 1, &num) || 1 != num)
        {
            printf("Choosing EGL config failed\n");
        }
    }

    EGLSurface egl_surf;
    {
        EGLint attr_list[] = {
            EGL_WIDTH, WINDOW_WIDTH,   //
            EGL_HEIGHT, WINDOW_HEIGHT, //
            EGL_NONE,                  //
        };
        egl_surf = eglCreatePbufferSurface(egl_disp, egl_config, attr_list);
        if (EGL_NO_SURFACE == egl_surf)
        {
            printf("Creating EGL surface failed\n");
        }
    }

    EGLContext egl_context;
    {
        EGLint attr_list[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2, //
            EGL_NONE,                      //
        };
        egl_context = eglCreateContext(egl_disp, egl_config, EGL_NO_CONTEXT, attr_list);
        if (EGL_NO_CONTEXT == egl_context)
        {
            printf("Creating EGL context fialed\n");
        }
    }

    eglMakeCurrent(egl_disp, egl_surf, egl_surf, egl_context);

    render();
    eglSwapBuffers(egl_disp, egl_surf);

    output_image();

    eglTerminate(egl_disp);

    return 0;
}
