
#version 100

attribute vec4 a_pos;
attribute mediump vec4 a_color;
attribute mediump vec2 a_tex;

varying mediump vec4 v_color;
varying mediump vec2 v_tex;

uniform mat4 m_trans;

void main()
{
    gl_Position = m_trans * a_pos;
    v_color = a_color;
    v_tex = a_tex;
}
