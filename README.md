# learn_gles2

Example programs from [https://learnopengl.com/](https://learnopengl.com/) re-implemented with OpenGL ES 2.0 in C.

Largely by the orignal order the examples are:

- Part 1: Rendering Pipeline
    1. [hello_window](hello_window)
    1. [hello_triangle](hello_triangle)
    1. [hello_vbo](hello_vbo)
    1. [shaders_varying](shaders_varying)
    1. [shaders_uniform](shaders_uniform)
    1. [shaders_interpolation](shaders_interpolation)
    1. [textures](textures) 
    1. [textures_combined](textures_combined)
    1. [textures_porter](textures_porter) (Porter-Duff algorithms, not in the original ones)
    1. [transform](transform)
    1. [transform_mvp](transform_mvp)
    1. [transform_cube](transform_cube)
    1. [transform_cube_many](transform_cube_many)
    1. [camera_circle](camera_circle)
    1. [camera_move](camera_move)
    1. [camera_look_around](camera_look_around)
- Part 2: Lighting
    1. TBD
    
