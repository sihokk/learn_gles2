
#version 100

// Required. Otherwise compiling will fail
// Fragment shaders do NOT have a default precision. Have to delcare explicitly
precision mediump float;

uniform vec4 a_color;

void main()
{
    gl_FragColor = a_color;
}
