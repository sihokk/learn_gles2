#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <GLES2/gl2.h>

#define INIT_SCREEN_WIDTH 320
#define INIT_SCREEN_HEIGHT 320

static char *read_shader(const char *file);
static bool load_shader(GLenum type, const char *path, GLuint *shader);
static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog);

static struct
{
    GLuint program;
    bool wireframe;
} render_state = {0};

static void on_resize(int32_t width, int32_t height)
{
    if (width > 0 && height > 0)
    {
        glViewport(0, 0, width, height);
    }
}

static void toggle_wireframe()
{
    render_state.wireframe = !render_state.wireframe;

    // TODO: ES does not support Polygon Mode
    if (render_state.wireframe)
    {
    }
    else
    {
    }
}

static void render_init()
{

    GLuint prog;
    if (!load_program("vertex_shader.glsl.c", "frag_shader.glsl.c", &prog))
    {
        printf("Loading program failed\n");
        return;
    }

    render_state.program = prog;

    const float vertices[] = {
        -.5f, .5f,  //
        -.5f, -.5f, //
        .5f, -.5f,  //
        .5f, .5f,   //
    };

    const GLushort indices[] = {
        0, 1, 2, //
        0, 2, 3, //
    };

    GLuint array_vbo, elem_vbo;
    {
        GLuint a1[2];
        glGenBuffers(2, a1);
        array_vbo = a1[0];
        elem_vbo = a1[1];
    }

    // As soon as `glBufferData()` is called vertex data is copied to VBOs
    glBindBuffer(GL_ARRAY_BUFFER, array_vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elem_vbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    GLint attr_index = glGetAttribLocation(prog, "a_pos");
    glEnableVertexAttribArray(attr_index);
    glVertexAttribPointer(attr_index, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glClearColor(.2f, .3f, .3f, 1.f);

    glUseProgram(prog);
}

static void render()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);
}

int main(int argc, char **argv)
{

    if (0 != SDL_Init(SDL_INIT_VIDEO))
    {
        printf("SDL init failed. \n");
        return -1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    SDL_Window *win = SDL_CreateWindow("Hello", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT,
                                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    if (NULL == win)
    {
        printf("SDL window creation failed. \n");
        SDL_Quit();
        return -1;
    }

    SDL_GLContext context = SDL_GL_CreateContext(win);
    if (NULL == context)
    {
        printf("SDL OpenGL context creation failed. \n");
        SDL_Quit();
        return -1;
    }

    glViewport(0, 0, INIT_SCREEN_WIDTH, INIT_SCREEN_HEIGHT);
    render_init();

    while (1)
    {

        render();
        SDL_GL_SwapWindow(win);

        SDL_Event e;
        if (0 == SDL_PollEvent(&e))
        {
            continue;
        }

        // ESC: quit
        if (SDL_KEYUP == e.type && SDLK_ESCAPE == e.key.keysym.sym)
        {
            break;
        }

        // M: toggle wireframe mode
        if (SDL_KEYUP == e.type && SDLK_m == e.key.keysym.sym)
        {
            toggle_wireframe();
            continue;
        }

        if (SDL_WINDOWEVENT == e.type)
        {
            // Window closed
            if (SDL_WINDOWEVENT_CLOSE == e.window.event)
            {
                break;
            }

            if (SDL_WINDOWEVENT_RESIZED == e.window.event)
            {
                on_resize(e.window.data1, e.window.data2);
            }
        }
    }

    SDL_Quit();

    return 0;
}

static bool load_shader(GLenum type, const char *path, GLuint *shader)
{
    GLuint shader_id = glCreateShader(type);
    char *src = read_shader(path);

    glShaderSource(shader_id, 1, &src, NULL);
    glCompileShader(shader_id);

    free(src);

    // Print compile log
    {
        GLint len;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetShaderInfoLog(shader_id, len, NULL, buf);

            printf("Compiling %s : %s\n", path, buf);
            free(buf);
        }
    }

    GLint result;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
    if (result)
    {
        // Success
        *shader = shader_id;
        return true;
    }
    else
    {
        glDeleteShader(shader_id);
        return false;
    }
}

static bool load_program(const char *vertex_shader_path, const char *frag_shader_path, GLuint *prog)
{

    GLuint prog_id = glCreateProgram();

    GLuint vertex_shader, frag_shader;

    if (!load_shader(GL_VERTEX_SHADER, vertex_shader_path, &vertex_shader))
    {
        printf("Loading vertex shader failed\n");
        glDeleteProgram(prog_id);
        return false;
    }

    if (!load_shader(GL_FRAGMENT_SHADER, frag_shader_path, &frag_shader))
    {
        printf("Loading fragment shader failed\n");
        glDeleteShader(vertex_shader);
        glDeleteProgram(prog_id);
        return false;
    }

    glAttachShader(prog_id, vertex_shader);
    glAttachShader(prog_id, frag_shader);
    glLinkProgram(prog_id);

    // It's safe to delete them
    glDeleteShader(vertex_shader);
    glDeleteShader(frag_shader);

    // Print linking log
    {
        GLint len;
        glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &len);
        if (len > 0)
        {
            char *buf = (char *)malloc(len);
            glGetProgramInfoLog(prog_id, len, NULL, buf);

            printf("Linking program : %s\n", buf);
            free(buf);
        }
    }

    GLint result;
    glGetProgramiv(prog_id, GL_LINK_STATUS, &result);
    if (result)
    {
        *prog = prog_id;
        return true;
    }
    else
    {
        glDeleteProgram(prog_id);
        return false;
    }
}

static char *read_shader(const char *file)
{

    SDL_RWops *in = SDL_RWFromFile(file, "r");
    if (NULL == in)
    {
    }

    const size_t delta = 512;

    char *buf = (char *)malloc(delta);
    size_t len = 0;
    size_t cap = delta;

    while (1)
    {
        size_t len1 = SDL_RWread(in, buf + len, 1, cap - len);
        if (0 == len1)
        {
            // TODO: may be error
            break;
        }

        len += len1;

        if (cap == len)
        {
            // Increase buffer
            char *buf1 = (char *)realloc(buf, cap + delta);
            if (NULL == buf1)
            {
                // TODO
            }

            buf = buf1;
            cap += delta;
        }
    }

    buf[len] = '\0';

    return buf;
}
